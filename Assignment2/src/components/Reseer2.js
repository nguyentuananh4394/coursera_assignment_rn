import React from 'react';
import {
    View, Text, Image, ImageBackground, Alert, Animated, Easing, Picker, Switch, DatePickerAndroid

} from 'react-native';
import { Header, Card, ListItem, Button, Rating, Icon, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker'
import * as Animatable from 'react-native-animatable';
import moment from 'moment';
class ReservationComponent extends React.Component {
    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
        this.state = {
            language: '',
            numberGuest: 1,
            smoking: false,
            datetime: '',
        }

    }
    componentDidMount() {
        this.animate()
    }
    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 8,
                duration: 8000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    /* MENTHOD */

    setDate(newDate) {
        this.setState({ chosenDate: newDate })
    }
    // reset values after click ok in alert
    resetValues() {
        this.setState({ smoking: false, numberGuest: 1, datetime: '' })
    }
    onPressSummit() {
        this.resetValues();
    }
    onPressRevervation() {
        var numberGuest = this.state.numberGuest === undefined ? 0 : this.state.numberGuest
        var smoking = this.state.smoking === undefined ? 'false' : 'true';
        var datetime = this.state.datetime === undefined ? '' : this.state.datetime;
        var message = 'Number of Guests: ' + numberGuest + '\n' + 'Smoking? ' + smoking + '\n' + 'Date and time: ' + datetime;
        Alert.alert(
            'Your Reservation OK',
            message,
            [
                { text: 'Cancel', onPress: () =>  this.resetValues(), style: 'cancel' },
                { text: 'OK', onPress: () => this.onPressSummit() },
            ],
            { cancelable: false }
        )
    }

    /* RENDER */
    render() {

        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Header
                    backgroundColor='#512DA8'
                    leftComponent={{ icon: 'chevron-left', color: '#fff', onPress: () => this.props.navigation.goBack() }}
                    centerComponent={{ text: 'Reverve Table', style: { color: '#fff',fontWeight:'bold' } }}
                />
                <Animatable.View animation="zoomIn" duration={1000} delay={1000}>
                    <Card containerStyle={{ borderWidth: 0, borderColor: 'white' }}>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row', height: 40, alignItems: 'center' }}>
                                <View style={{ flex: 1 }}>
                                    <Text>Number of Guests</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <Picker
                                        selectedValue={this.state.numberGuest}
                                        style={{ height: 30, justifyContent: 'center', width: '100%', color: 'black' }}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ numberGuest: itemValue })}>
                                        <Picker.Item label="1" value="1" />
                                        <Picker.Item label="2" value="2" />
                                        <Picker.Item label="3" value="3" />
                                        <Picker.Item label="4" value="4" />
                                        <Picker.Item label="5" value="5" />
                                        <Picker.Item label="6" value="6" />
                                    </Picker>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', height: 40, alignItems: 'center' }}>
                                <View style={{ flex: 2 }}>
                                    <Text>Smoking/Non-Smoking</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <Switch value={this.state.smoking} onValueChange={() => this.setState({ smoking: !this.state.smoking })}></Switch>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', height: 40, alignItems: 'flex-end' }}>
                                <View style={{ flex: 2 }}>
                                    <Text>Date and Time</Text>
                                </View>
                                <View style={{ flex: 3.5 }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.datetime}
                                        mode="datetime"
                                        placeholder="select date"
                                        format=""
                                        minDate={moment().format('MMMM Do YYYY, h:mm:ss a')}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                        }}
                                        onDateChange={(date) => { this.setState({ datetime: date }) }}
                                    />
                                </View>
                            </View>
                            <Button
                                title='RESERVE'
                                titleStyle={{ fontWeight: 'bold', color: 'white' }}
                                buttonStyle={{ backgroundColor: '#512DA8' }}
                                containerStyle={{ marginTop: 20 }}
                                onPress={() => this.onPressRevervation()}
                            />
                        </View>
                    </Card>
                </Animatable.View>


            </View>
        )
    }
};

const mapStateToProps = state => {
    return {
        comment: state.comment
    }
}

export default connect(mapStateToProps, null)(ReservationComponent);
