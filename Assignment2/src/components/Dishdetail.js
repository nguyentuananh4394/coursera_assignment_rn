import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, StyleSheet, Modal, PanResponder, Alert } from 'react-native';
import { Card, Icon, Rating, Button, Input } from 'react-native-elements';

import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreator';
// import * as Animatable from 'react-native-reanimated';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (commentDetail) => dispatch(postComment(commentDetail))
})


function RenderComments(props) {
    const comments = props.comments;
    const renderCommentItem = ({ item, index }) => {
        return (
            <View key={index} style={{ margin: 10 }}>
                <Text style={{ fontSize: 14 }}>{item.comment}</Text>
                <Text style={{ fontSize: 12 }}>{item.rating} Stars</Text>
                <Text style={{ fontSize: 12 }}>{'-- ' + item.author + ', ' + item.date} </Text>
            </View>
        );
    };

    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
            <Card title='Comments' >
                <FlatList
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}

function RenderModal(props) {
    let formValues = {
        author: '',
        comment: '',
        rating: 3,
        date: '',
        dishId: props.dishId,
    };
    const submit = () => {
        formValues.date = new Date();
        props.onSubmit(formValues);
    }
    return (
        <Modal
            animationType={"slide"} transparent={false}
            visible={props.showModal}
            onRequestClose={() => toggleModal()}>
            <View style={styles.modal}>
                <Rating
                    showRating
                    type="star"
                    fractions={1}
                    startingValue={formValues.rating}
                    imageSize={40}
                    onFinishRating={(star) => { formValues.star = star }}
                    onStartRating={this.ratingStarted}
                    style={{ paddingVertical: 10 }}
                />
                <View style={{ flexDirection: 'row' }}>
                    <Input
                        placeholder='Author'
                        onChangeText={(author) => { formValues.author = author }}
                        inputStyle={styles.modalInput}
                        leftIcon={
                            <Icon
                                raised
                                reverse
                                name={'user'}
                                type='font-awesome'
                                size={15}
                            />
                        }
                    />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Input
                        placeholder="Comment"
                        onChangeText={(comment) => { formValues.comment = comment }}
                        inputStyle={styles.modalInput}
                        leftIcon={
                            <Icon
                                raised
                                reverse
                                name={'comments'}
                                type='font-awesome'
                                size={15}
                            />
                        }
                    />
                </View>
                <View>
                    <Button
                        large
                        onPress={() => submit()}
                        title="SUBMIT"
                        buttonStyle={{
                            backgroundColor: "#512DA8",
                            height: 45,
                            borderColor: "transparent",
                            borderWidth: 0,
                            borderRadius: 5,
                            marginTop: 30
                        }}
                    />
                    <Button
                        large
                        onPress={() => props.toggleModal()}
                        title="CANCEL"
                        buttonStyle={{
                            backgroundColor: "grey",
                            height: 45,
                            borderColor: "transparent",
                            borderWidth: 0,
                            borderRadius: 5,
                            marginTop: 30
                        }}
                    />
                </View>
            </View>
        </Modal>
    );
}

function RenderDish(props) {
    const dish = props.dish;
    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        if (dx < -200)
            return true;
        else
            return false;
    }

    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        if (dx > 200)
            return true;
        else
            return false;
    }
    handleViewRef = ref => this.view = ref;

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gesture) => {
            return true;
        },
        onPanResponderGrant: () => { this.view.rubberBand(1000).then(endState => console.log(endState.finished ? 'finished' : 'cancelled')); },
        onPanResponderEnd: (e, gestureState) => {
            console.log('pan responder end', gestureState);
            if (recognizeDrag(gestureState)) {
                Alert.alert(
                    'Add Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to favorite?',
                    [
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        { text: 'OK', onPress: () => { props.favorite ? console.log('Already favorite') : props.onPress() } },
                    ],
                    { cancelable: false }
                )
                return true;
            }
            if (recognizeComment(gestureState)) {
                props.onPressAddComment();
                return true;
            }

        }
    });

    if (dish != null) {
        return (
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
                ref={this.handleViewRef}
                {...panResponder.panHandlers}>
                <Card
                    featuredTitle={dish.name}
                    image={{ uri: baseUrl + dish.image }}>
                    <Text style={{ margin: 10 }}>
                        {dish.description}
                    </Text>
                    <View style={styles.buttons}>
                        <Icon
                            raised
                            reverse
                            name={props.favorite ? 'heart' : 'heart-o'}
                            type='font-awesome'
                            color="#f50"
                            onPress={() => props.favorite ? console.log('Already favourite') : props.onPress()}
                        />
                        <Icon
                            raised
                            reverse
                            name={'pencil'}
                            type='font-awesome'
                            color="#512DA8"
                            onPress={() => props.onCommentPress()}
                        />
                    </View>
                </Card>
            </Animatable.View>
        );
    }
    else {
        return (
            <View></View>
        );
    }
}

const styles = StyleSheet.create({
    buttons: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    modal: {
        justifyContent: 'center',
        margin: 20,
        marginTop: 40
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    },
    modalInput: {
        marginTop: 20,
        fontSize: 20,
    }
})

class Dishdetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        };
    }
    markFavorite(dishId) {
        this.props.postFavorite(dishId);
    }
    static navigationOptions = {
        title: 'Dish Details'
    };
    toggleModal() {
        this.setState({ showModal: !this.state.showModal });
    }
    handleComment(commentDetail) {
        /* commentDetail = { author, comment, dishId, date, rating } */
        this.toggleModal();
        this.props.postComment(commentDetail);
    }
    render() {
        const dishId = this.props.navigation.getParam('dishId', '');
        return (
            <ScrollView>
                {/* <RenderDish dish={this.props.dishes.dishes[+dishId]}
                    favorite={this.props.favorites.some(el => el === dishId)}
                    onPress={() => this.markFavorite(dishId)}
                    onCommentPress={() => this.toggleModal()}
                    onPressAddComment={ () => this.toggleModal() }
                />
                <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />
                 */}
                <RenderModal
                    showModal={this.state.showModal}
                    dishId={dishId}
                    toggleModal={() => { this.toggleModal() }}
                    onSubmit={(commentDetail) => { this.handleComment(commentDetail) }}
                />
            </ScrollView>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);