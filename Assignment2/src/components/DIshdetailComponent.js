// import React from 'react';
// import {
//     View, Text, Image, ImageBackground, TouchableOpacity, Animated, Easing, PanResponder, Alert,Modal
// } from 'react-native';
// import { Header, Card, ListItem, Button, Rating, Icon } from 'react-native-elements';
// import { connect } from 'react-redux';
// import * as Animatable from 'react-native-animatable';

// class DishdetailComponent extends React.Component {
//     constructor(props) {
//         super(props);
//         this.animatedValue = new Animated.Value(0);
//         this.state = {
//             listComment: [],
//             modalVisible:false
//         }
//     }
//     /* MENTHOD */
//     onPressDetail() {
//         this.props.navigation.navigate("ReservationScreen");
//         // Alert.alert("click next screen");
//     }
//     onPressHeart() {

//     }
//     handleViewRef = ref => this.view = ref;

//     componentDidMount() {
//         this.setState({modalVisible:true})
//         this.animate()
//     }

//     animate() {
//         this.animatedValue.setValue(0)
//         Animated.timing(
//             this.animatedValue,
//             {
//                 toValue: 8,
//                 duration: 8000,
//                 easing: Easing.linear
//             }
//         ).start(() => this.animate())
//     }

//     /* RENDER */
//     render() {
//         const panResponder = PanResponder.create({
//             onStartShouldSetPanResponder: (e, gestureState) => {
//                 return true;
//             },
//             onPanResponderGrant: () => { this.view.rubberBand(1000).then(endState => console.log(endState.finished ? 'finished' : 'cancelled')); },
//             onPanResponderEnd: (e, gestureState) => {
//                 return true;
//             }
//         })

//         return (
//             <View style={{ backgroundColor: 'white', flex: 1 }}>
//             <Modal animationType='slide' transparent={false}  visible={this.state.modalVisible}>
//             <Header
//                     backgroundColor='#512DA8'
//                     leftComponent={{ icon: 'chevron-left', color: '#fff', onPress: () => this.props.navigation.goBack() }}
//                     centerComponent={{ text: 'DishDetail', style: { color: '#fff', fontWeight: 'bold' } }}
//                 />
//                 <Animatable.View animation="fadeInDown" duration={1000} delay={1000}
//                     ref={this.handleViewRef}
//                     {...panResponder.panHandlers}
//                 >
//                     <Card containerStyle={{ height: 200, flexDirection: 'column' }}>
//                         <View style={{ height: '60%', resizeMode: 'center' }}>
//                             <ImageBackground style={{ flex: 1 }} source={{ uri: 'https://www.cicis.com/media/1138/pizza_trad_pepperoni.png' }}>
//                                 <View style={{ flex: 1, alignItems: 'center' }}>
//                                     <Text style={{ zIndex: 50, flex: 1, color: 'white', alignItems: 'center', justifyContent: 'center', fontSize: 16, fontWeight: 'bold' }}>Uthappizza</Text>
//                                 </View>
//                             </ImageBackground>
//                         </View>

//                         <View style={{ flex: 1, flexDirection: 'column' }}>
//                             <Text style={{ fontSize: 8 }}>A quique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe{'\n'} vine cherry tomatoes,Vidalia onion, Guntur chillies and Buffalo Paneer </Text>
//                             <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
//                                 <Icon
//                                     component={TouchableOpacity}
//                                     reverse
//                                     name='heart'
//                                     type='font-awesome'
//                                     color='#FF5502'
//                                     onPress={() => this.onPressHeart()}
//                                 />
//                                 <Icon
//                                     component={TouchableOpacity}
//                                     reverse
//                                     name='pencil'
//                                     type='font-awesome'
//                                     color='#512DA8'
//                                     onPress={() => this.onPressDetail()}
//                                 />
//                             </View>
//                         </View>
//                     </Card>

//                 </Animatable.View>

//                 <Animatable.View animation="fadeInUp" duration={1000} delay={1000}>
//                     <Card containerStyle={{ padding: 0 }} title="Comments" >
//                         {
//                             this.props.comment.comment.map((u, i) => {
//                                 return (
//                                     <ListItem
//                                         key={i}
//                                         title={u.comment}
//                                         subtitle={
//                                             <View style={{ flexDirection: 'column' }}>
//                                                 <Rating
//                                                     type="star"
//                                                     startingValue={u.rating}
//                                                     readonly
//                                                     imageSize={10}
//                                                     style={{ paddingVertical: 10, alignItems: 'flex-start' }}
//                                                 />
//                                                 <Text>--{u.time}</Text>
//                                             </View>
//                                         }
//                                     />
//                                 );
//                             })
//                         }
//                     </Card>
//                 </Animatable.View>
//             </Modal>
//             </View>
//         )
//     }
// };

// const mapStateToProps = state => {
//     return {
//         comment: state.comment
//     }
// }

// export default connect(mapStateToProps, null)(DishdetailComponent);
