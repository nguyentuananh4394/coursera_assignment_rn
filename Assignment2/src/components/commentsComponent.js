import React from 'react';
import {
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Rating, FormLabel, Button } from 'react-native-elements';
import { connect } from 'react-redux';
import {  postComment } from '../redux/ActionCreator'
class CommentScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 0,
            author: '',
            comment: '',
            time: ''
        }
    }
    /* METHOD */
    ratingCompleted = (ratingCurrent) => {
        this.setState({ rating: ratingCurrent })
    }

    onPressSummitForm() {
        var d = new Date();
        const objectComment = { comment: this.state.comment, rating: this.state.rating, time: d }
        const myObjStr = JSON.stringify(objectComment);
        this.props.postComment(myObjStr);
        this.props.navigation.goBack();
    }

    /* RENDER */
    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1, flexDirection: 'column' }}>
                <View style={{ justifyContent: 'center', marginTop: 24, alignItems: 'center', flexDirection: 'column' }}>
                    <Rating
                        startingValue={this.state.rating}
                        showRating
                        type="star"
                        fractions={1}
                        imageSize={40}
                        onFinishRating={this.ratingCompleted}
                        style={{ paddingVertical: 10 }}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <Input
                        placeholder='Author'
                        value={this.state.author}
                        onChangeText={(text) => this.setState({ author: text.trim() })}
                        containerStyle={{ width: '100%' }}
                        leftIcon={
                            <Icon
                                name='user'
                                size={24}
                                color='black'
                            />
                        }
                    />
                    <Input
                        placeholder='Comment'
                        onChangeText={(text) => this.setState({ comment: text.trim() })}
                        containerStyle={{ width: '100%' }}
                        leftIcon={
                            <Icon
                                name='comment'
                                size={24}
                                color='black'
                            />
                        }
                    />
                    <View style={{ margin: 10 }}>
                        <Button
                            title='SUBMIT'
                            titleStyle={{ fontWeight: 'bold', color: 'white' }}
                            buttonStyle={{ backgroundColor: '#512DA8' }}
                            containerStyle={{ marginTop: 20 }}
                            onPress={() => this.onPressSummitForm()}
                        />
                        <Button
                            title='CANCEL'
                            buttonStyle={{ backgroundColor: 'gray' }}
                            containerStyle={{ marginTop: 20 }}
                            titleStyle={{ fontWeight: 'bold', color: 'white' }}
                            onPress={() => this.props.navigation.goBack()}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    postComment: (comments) => dispatch(postComment(comments)),
})

export default connect(null, mapDispatchToProps)(CommentScreen);