import React from 'react';
import {
    View, Text, TouchableOpacity,
    FlatList, ImageBackground, ScrollView
} from 'react-native';
import Loading from '../components/LoadingComponent'
import { Tile } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';

class Menu extends React.Component {
    static navigationOptions = {
        title: 'Menu'
    }

    constructor(props) {
        super(props);
        this.state = {
            dishes: ''
        }
    }

    render() {
        const renderMenuItem = ({ item, index }) => {
            return (
                <Tile
                    key={index}
                    title={item.name}
                    caption={item.description}
                    featured
                    onPress={() => navigate('Dishdetail', { dishId: item.id })}
                    imageSrc={{ uri: baseUrl + item.image }}
                />
            )
        }
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <FlatList
                    data={this.props.dishes.dishes}
                    renderItem={renderMenuItem}
                    keyExtractor={item => item.id.toString()}
                />
            </View>

        );
    }
}
const mapStateToProps = state => {
    return {
        dishes: state.dishes
    }
}

export default connect(mapStateToProps)(Menu);