

export default {
  txtLoading: 'Đang tải',

  txtMottoForget: 'Vui lòng nhập E-mail mà bạn đã sử dụng để đăng ký trên hệ thống.',
  txtGoBackForget: 'Quay lại',

  /**
   * LOGIN SCREEN
   **/
  txtLoginUser: 'Tên đăng nhập',
  txtLoginPass: 'Mật khẩu',
  txtLoginSavePass: 'Lưu mật khẩu',
  txtLoginForgetPass: 'Quên mật khẩu?',
  txtLoginButton: 'ĐĂNG NHẬP',

  /**
   * HOME TAB TITLE 
   **/
  txtTab1: 'Trang chủ',
  txtTab2: 'Tin nhắn',
  txtTab3: 'Tin tức',
  txtTab4: 'Thông báo',

  /**
   * HOME SCREEN
   **/
  txtNotiHeader1: 'Thông báo',
  txtNotiHeader2: 'Xem tất cả',
  txtQuick1: 'Thực đơn',
  txtQuick1_1: 'Ăn sáng (7h-8h)',
  txtQuick2: 'Thời khóa biểu',
  txtQuick2_1: 'Âm nhạc (8h10-8h20)',
  txtQuick3: 'Xin nghỉ',
  txtQuick4: 'Sức khỏe',
  txtQuick5: 'Điểm danh',

  txtAlbumHeader1: 'Album',
  txtAlbumHeader2: 'Xem tất cả',

  /**
   * PROFILE SCREEN
   **/
  txtProfileTitle: 'Tài khoản',
  txtChangePassTitle: 'Đổi mật khẩu',
  txtMyInfoTitle: 'Thông tin cá nhân',

  /**
   * MESSAGE SCREEN
   **/
  txtMessageTitle_Teacher: 'Giáo viên',
  txtMessageTitle_Other: 'Khác',

  /**
   * ABSENT SCREEN
   **/
  txtAbsentTitle_1: 'Đơn xin nghỉ',
  txtAbsentTitle_2: 'Số buổi đã nghỉ',
  txtAbsentFrom: 'Từ',
  txtAbsentTo: 'Đến',
  txtAbsentMessage: 'Điền thông tin xin nghỉ của bé...',
  txtAbsentSend: 'GỬI',

  /**
   * ALBUM DETAIL SCREEN
   **/
  txtAlbumDetailLike: 'Thích',
  txtAlbumDetailComment: 'Bình luận',
  
  /**
   * HEALTH DETAIL SCREEN
   **/
  txtHealthTitle: 'Sức khỏe',

  /**
  * MENU DRAWER
  **/
  txtDrawerNoti: 'Thông báo',
  txtDrawerNews: 'Tin tức',
  txtDrawerMenu: 'Thực đơn',
  txtDrawerSchedule: 'Thời khóa biểu',
  txtDrawerAbsent: 'Xin nghỉ',
  txtDrawerHealth: 'Sức khỏe',
  txtDrawerTeach: 'Dạy trẻ',
  txtDrawerAlbum: 'Hình ảnh',
  txtDrawerAttendant: 'Điểm danh',
  txtDrawerContact: 'Liên hệ',
  txtDrawerShare: 'Chia sẻ',
  txtDrawerPhone: '(+84) 983 870 342',
  txtDrawerInfo: 'eKidPro - ZiniMedia',
  txtDrawerVersion: 'Version 1.0',
  txtDrawerTCP: 'T&C - Privacy',
   /**
  * Attendant
  **/
 txtAttendantTitle_1:'Danh sách học sinh đi học',
 txtAttendantTitle_2:'Danh sách học sinh nghỉ học',

} 