

export default {
  primaryApp: '#2FA6DE', // Xanh da troi

  primaryButton: '#ED8E27', // Cam

  primaryTextNote: '#E82879', // Hong

  backgroundMain: '#F6F7F9', // Xam background
  
  borderColor: '#979797',

  cor_xam: '#333333',

  placeholderTextColor: '#C4C4C4', // Xam place holder

  text_1: '#525669',
  text_2: '#777777',
  
  backgroundColorNote: '#ECECEC'
} 