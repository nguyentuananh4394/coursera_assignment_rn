import { Dimensions, PixelRatio } from 'react-native';
import moment from 'moment';

const STANDARD_SIZE = {
  width: 375
}
const widthPercentageToDP = widthPercent => {
    let screenWidth = Dimensions.get('window').width;
    // Convert string input to decimal number
    let elemWidth = parseFloat(widthPercent);
    return PixelRatio.roundToNearestPixel(screenWidth * elemWidth / 100);
  };
  
  const heightPercentageToDP = heightPercent => {
    let screenHeight = Dimensions.get('window').height;
    // Convert string input to decimal number
    let elemHeight = parseFloat(heightPercent);
  return PixelRatio.roundToNearestPixel(screenHeight * elemHeight / 100);
  };
  
  const getFontSize = (size) => {
    return (parseInt(size) * Dimensions.get('window').width / STANDARD_SIZE.width);
  }
  
  const getTimeWithNow = (timestamp) => {
    try {
      let _tmpTime = moment.unix(timestamp/1000).format('DD/MM/YYYY');
      let _tmpNow = moment().format('DD/MM/YYYY');
      
      if (_tmpTime < _tmpNow) {
        return {
          time: moment.unix(timestamp/1000).format('DD/MM/YYYY'),
          type: 'days'
        };
      } else if (_tmpTime == _tmpNow) {
        _tmpTime = moment.unix(timestamp/1000).format('YYYY-MM-DD HH:mm');
        _tmpNow =  moment();
        let result = {
          time: _tmpNow.diff(_tmpTime, 'hours'),
          type: 'hours'
        }
        if (result.time == 0) {
          result = {
            time: _tmpNow.diff(_tmpTime, 'minutes'),
            type: 'minutes'
          };
        }
        return result;
      }
    } catch (e) {
      console.log('getTimeWithNow error: ', e);
      return null;
    }
  }
  

export default {
    winSize: Dimensions.get('window'),
  
    // SCALE SIZE
    w_scale: widthPercentageToDP,
    h_scale: heightPercentageToDP,
  
    // FONT
    getFontSize: getFontSize,
    // fontBold: 'Roboto-Bold', 
    // fontItalic: 'Roboto-Italic',
    // fontRegular: 'Roboto-Regular',
    // fontLight: 'Roboto-Light',
    // fontLightItalic: 'Roboto-LightItalic',
    // fontMedium: 'Roboto-Medium',
  
    // TIME
    getTimeWithNow: getTimeWithNow
    
  }