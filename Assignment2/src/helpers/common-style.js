export default {
  /* MARGIN */
  mv_5: { marginVertical: 5 },
  mv_10: { marginVertical: 10 },
  
  mh_5: { marginHorizontal: 5 },
  mh_10: { marginHorizontal: 10 },

  mt_5: { marginTop: 5 },
  mb_5: { marginBottom: 5 },
  ml_5: { marginLeft: 5 },
  mr_5: { marginRight: 5 },

  mt_10: { marginTop: 10 },
  mb_10: { marginBottom: 10 },
  ml_10: { marginLeft: 10 },
  mr_10: { marginRight: 10 },

  /* PADDING */
  pv_5: { paddingVertical: 5 },
  pv_10: { paddingVertical: 10 },
  
  ph_5: { paddingHorizontal: 5 },
  ph_10: { paddingHorizontal: 10 },

  pall_5: { padding: 5 },
  pall_10: { padding: 10 },

  pt_5: { paddingTop: 5 },
  pb_5: { paddingBottom: 5 },
  pl_5: { paddingLeft: 5 },
  pr_5: { paddingRight: 5 },

  pt_10: { paddingTop: 10 },
  pb_10: { paddingBottom: 10 },
  pl_10: { paddingLeft: 10 },
  pr_10: { paddingRight: 10 },
}