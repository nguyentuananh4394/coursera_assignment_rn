import React, { Component } from 'react';
import Menu from '../components/Menu';
import Dishdetail from '../components/Dishdetail';
import Home from '../components/HomeComponent';
import Contact from '../components/ReservationComponent';
import About from '../components/AboutComponent';
import Login from '../components/LoginComponent';
import CommentScreen from '../components/commentsComponent';
import ReservationScreen from '../components/ReservationComponent';
import { View, Platform, StyleSheet, Image, ScrollView, Text } from 'react-native';
import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchDishes, fetchComments, fetchPromos, fetchLeaders } from '../redux/ActionCreator';

const HomeNavigator = createStackNavigator({
  Home: { screen: Home }
}, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#fff"
    })
  });

const ContactNavigator = createStackNavigator({
  Contact: { screen: Contact }
}, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#fff"
    })
  });
const ReservationNavigator = createStackNavigator({
  ReservationScreen: { screen: ReservationScreen, navigationOptions: { header: null } }
}, {
    navigationOptions: ({ navigation }) => ({
      header: null,
      headerStyle: {
        backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#fff"
    })
  });


const AboutNavigator = createStackNavigator({
  About: { screen: About }
}, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#fff"
    })
  });
  const LoginNaivgator = createStackNavigator({
    Login: { screen: Login }
  }, {
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: "#512DA8"
        },
        headerTitleStyle: {
          color: "#fff"
        },
        headerTintColor: "#fff"
      })
    });


const CustomDrawerContentComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{ flex: 1 }}>
          {/* <Image source={require('../../../json-server/public/images/logo.png')} style={styles.drawerImage} /> */}
        </View>
        <View style={{ flex: 2 }}>
          <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
        </View>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

const MenuNavigator = createStackNavigator({
  Menu: { screen: Menu },
  CommentScreen: { screen: CommentScreen, navigationOptions: { header: null } },
  ReservationScreen: { screen: ReservationScreen, navigationOptions: { header: null } },
  Dishdetail: { screen: Dishdetail, navigationOptions: { header: null } }
},
  {
    initialRouteName: 'Menu',
    navigationOptions: {
      header: null,
    }
  }
);

const MainNavigator = createDrawerNavigator({
  Menu: {
    screen: Menu,
    navigationOptions: {
      title: 'Menu',
      drawerLabel: 'Menu'
    },
  },
  Login: {
    screen: LoginNaivgator,
    navigationOptions: {
      title: 'Login',
      drawerLabel: 'Login'
    },
  },
  Home: {
    screen: HomeNavigator,
    navigationOptions: {
      title: 'Home',
      drawerLabel: 'Home'
    }
  },
  About: {
    screen: AboutNavigator,
    navigationOptions: {
      title: 'About Us',
      drawerLabel: 'About Us'
    }
  },

  Contact: {
    screen: ContactNavigator,
    navigationOptions: {
      title: 'Contact Us',
      drawerLabel: 'Contact Us'
    }
  },
  ReservationScreen: {
    screen: ReservationNavigator,
    navigationOptions: {
      title: 'Reservation',
      drawerLabel: 'Reservation'
    }
  },
}, {
    drawerBackgroundColor: '#D1C4E9',
    initialRouteName:'Login',
    contentComponent: CustomDrawerContentComponent
  });

class MainComponent extends Component {

  componentDidMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <MainNavigator />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders
  }
}
const mapDispatchToProps = dispatch => ({
  fetchDishes: () => dispatch(fetchDishes()),
  fetchComments: () => dispatch(fetchComments()),
  fetchPromos: () => dispatch(fetchPromos()),
  fetchLeaders: () => dispatch(fetchLeaders()),
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerHeader: {
    backgroundColor: '#512DA8',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  drawerHeaderText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold'
  },
  drawerImage: {
    margin: 10,
    width: 80,
    height: 60
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);
