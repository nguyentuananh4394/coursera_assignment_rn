/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation'
import RootStack from './src/navigation/router';
import {Provider} from 'react-redux'
import { ConfigureStore } from './src/redux/configureStore';
// const AppContainer = createStackNavigator(RootStack);
// const AppNavigator = createAppContainer(RootStack);
const store = ConfigureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
    );
  }
}
console.disableYellowBox = true;
export default App;
